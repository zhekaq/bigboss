<?php

namespace App\Http\Controllers;

use App\Employees;
use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\EmployeesRepository;

class EmployeesController extends Controller
{
    /**
     * The task repository instance.
     *
     * @var EmployeesRepository
     */
    protected $employees;

    /**
     * Create a new controller instance.
     *
     * @param  EmployeesRepository  $employees
     * @return void
     */
    public function __construct(EmployeesRepository $employees)
    {
        $this->employees = $employees;
    }

    /**
     * Display a list of all of the user's task.
     *
     * @param  Request  $request
     * @return Response
     */
    public function index($id=0)
    {

        $myTree =$this->employees->getChildrenById($id);

        /*if($myTree) {
            for ($i = 0; $i < count($myTree); $i++) {
                $myTree[$i]["childrens"] = $this->employees->getChildrenById($myTree[$i]->id);
                if ($myTree[$i]["childrens"]) {
                    for ($j = 0; $j < count($myTree[$i]["childrens"]); $j++) {
                        if ($this->employees->getChildrenByIdLimOne($myTree[$i]["childrens"][$j]->id)) {
                            $myTree[$i]["childrens"][$j]["leafs"] = true;
                        } else {
                            $myTree[$i]["childrens"][$j]["leafs"] = false;
                        }
                    }
                }
            }
        }*/

         if($myTree) {
            $i=0;
            foreach ($myTree as $curTreEl) {
                $myTree[$i]->{"childrens"} = $this->employees->getChildrenById($curTreEl->id);
                if ($myTree[$i]->childrens) {
                    $j=0;
                    foreach ($myTree[$i]->childrens as $child) {
                        if ($this->employees->getChildrenByIdLimOne($child->id)) {
                            $myTree[$i]->childrens[$j]->{'leafs'} = true;
                        } else {
                            $myTree[$i]->childrens[$j]->{'leafs'} = false;
                        }
                        $j++;
                    }
                }
                $i++;
            }
        }

        $result=$myTree;

        return view('employees', [
            'employees' => $result,
        ]);
    }
}
