<?php
namespace App\Repositories;

use App\Employees;
use Illuminate\Support\Facades\DB;

class EmployeesRepository
{
    /**
     * Get all of the tasks for a given user.
     *
     * @return Collection
     */
    public function allEmployees()
    {
        return Employees::orderBy('id', 'asc')
            ->get();
    }

    public function firstEmployees()
    {
        return Employees::orderBy('id', 'asc')
            ->take(1)
            ->get();
    }

    public function firstLvlEmployees()
    {
        return Employees::where('supervisor_code','0')
            ->orderBy('id', 'asc')
            ->get();
    }

    public function getChildrenById($id)
    {
        return DB::select('SELECT e.id, e.fio, e.supervisor_code, positions.position_name FROM employees as e LEFT JOIN positions ON e.position_code=positions.id WHERE e.supervisor_code = '.$id);
    }

    public function getChildrenByIdLimOne($id)
    {
        return Employees::where('supervisor_code',$id)
            ->orderBy('id', 'asc')
            ->take(1)
            ->get();
    }

}