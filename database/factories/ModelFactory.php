<?php
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Employees::class, function (Faker\Generator $faker) {
    return [
        'fio'=> $faker->name,
        'come_date'=> Carbon::create(2015, 5, 28, 0, 0, 0),
        'current_payment'=>rand(2000,50000)
    ];
});

$factory->defineAs(App\Employees::class, 'level_1', function ($faker) use ($factory) {
    $employee = $factory->raw(App\Employees::class);

    return array_merge($employee, [
        'position_code'=>1,
        'supervisor_code'=>0,
    ]);
});

$factory->defineAs(App\Employees::class, 'level_2', function ($faker) use ($factory) {
    $employee = $factory->raw(App\Employees::class);

    return array_merge($employee, [
        'position_code'=>2,
        'supervisor_code'=>rand(1,10),
    ]);
});

$factory->defineAs(App\Employees::class, 'level_3', function ($faker) use ($factory) {
    $employee = $factory->raw(App\Employees::class);

    return array_merge($employee, [
        'position_code'=>3,
        'supervisor_code'=>rand(11,110),
    ]);
});

$factory->defineAs(App\Employees::class, 'level_4', function ($faker) use ($factory) {
    $employee = $factory->raw(App\Employees::class);

    return array_merge($employee, [
        'position_code'=>4,
        'supervisor_code'=>rand(110,1110),
    ]);
});

$factory->defineAs(App\Employees::class, 'level_5', function ($faker) use ($factory) {
    $employee = $factory->raw(App\Employees::class);

    return array_merge($employee, [
        'position_code'=>5,
        'supervisor_code'=>rand(1110,11110),
    ]);
});



