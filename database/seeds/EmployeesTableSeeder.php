<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class EmployeesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Employees::class,'level_1',10)->create();
        factory(App\Employees::class,'level_2',100)->create();
        factory(App\Employees::class,'level_3',1000)->create();
        factory(App\Employees::class,'level_4',10000)->create();
        factory(App\Employees::class,'level_5',40000)->create();

    }
}
