<?php

use Illuminate\Database\Seeder;

class PositionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('positions')->insert([
            'position_name'=>'Boss'
        ]);
        DB::table('positions')->insert([
            'position_name'=>'Deputy Director'
        ]);
        DB::table('positions')->insert([
        'position_name'=>'Manager'
        ]);
        DB::table('positions')->insert([
            'position_name'=>'Team Leader'
        ]);
        DB::table('positions')->insert([
            'position_name'=>'Worker'
        ]);
    }
}
