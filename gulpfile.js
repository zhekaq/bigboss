/**
 * Created by zhekaq on 02.07.18.
 */
var elixir = require('laravel-elixir');
elixir(function(mix) {
    mix.sass([
        'app.scss'
    ], 'public/css');
});