
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

// require('./bootstrap');
//
// window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

function loadNextTree() {
    if($(this).attr('id'))
    {
        var id = $(this).attr('id');
        id = id.match(/\d+/);
        console.log(id);
        console.log("---link= /" + id);
        $.ajax({
            url: "/" + id,
            success: function (result) {
                $("#tree-" + id).append($(result).find('#tree-container'));
                //console.log(result);
            }
        });
        $(this).removeClass("trhas-childs");
        $(this).unbind();

    }
}

$(document).ready(function(){


    $(document).on("click", '.trhas-childs', function loadNextTree() {
        if($(this).attr('id'))
        {
            var id = $(this).attr('id');
            id = id.match(/\d+/);
            $.ajax({
                url: "/" + id,
                success: function (result) {
                    $("#tree-" + id).append($(result).find('#tree-container'));
                    //console.log(result);
                }
            });
            $(this).removeClass("trhas-childs");
            $(this).unbind();


        }
    });

});
