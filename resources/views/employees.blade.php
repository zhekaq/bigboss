@extends('layouts.app')

@section('content')
    <!-- Create Task Form... -->
    <!-- Current Tasks -->
    <div id="app"></div>
    @if (count($employees) > 0)
        <div class="panel panel-default">
            <div class="panel-body">
                <div id="jstree">
                    <ul id="tree-container">

                    @foreach ($employees as $employee)
                        <li id="{{ $employee->id }}">

                            <span>{{ $employee->fio }}</span>
                            <span>({{ $employee->position_name }})</span>

                            <ul>

                                    @foreach ($employee->childrens as $child)
                                    <li id="tree-{{ $child->id }}"
                                    @php
                                    if($child->leafs){
                                        echo 'class="trhas-childs"';
                                        }
                                    @endphp
                                    >
                                        <span>{{ $child->fio }}</span>
                                        <span>({{ $child->position_name }})</span>
                                    </li>
                                    @endforeach
                            </ul>
                        </li>
                    @endforeach

                    </ul>
                </div>
            </div>
        </div>
    @endif

@endsection