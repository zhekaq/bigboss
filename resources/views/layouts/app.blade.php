<!DOCTYPE html>
<html lang="en">
<head>
    <title>Laravel Quickstart - Basic</title>

    <!-- CSS And JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.1/jquery.min.js"></script>
    <script src="{{ URL::asset('js/jstree.min.js') }}"></script>
    <script src="{{ URL::asset('js/app.js') }}"></script>

    <link rel="stylesheet" href="{{ URL::asset('css/jstree/default/style.min.css') }}" />
    <!-- Styles -->
    <link href="{{ URL::asset('css/app.css') }}" rel="stylesheet">

</head>

<body >
{{--<div class="container">
    <nav class="navbar navbar-default">
        <!-- Navbar Contents -->
    </nav>
</div>--}}
<h1>Employees</h1>
@yield('content')
</body>
</html>